import { AppBar, Button, List, ListItem, ListItemText, makeStyles, Toolbar, Typography, withStyles } from '@material-ui/core';
import React, { FC } from 'react';
import {ReactComponent as Logo} from '../logo.svg';

export interface HeaderBarProps {

}

const useStyles = makeStyles(() => ({
    root:{
        height: 120,
        justifyContent: 'center'
    },
    title: {
        fontWeight: 'bold'
    },
    navigation: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
    }
}));

const CenteredListItemText = withStyles({
    root: {
        textAlign: 'center'
    }
})(ListItemText);

const HeaderBar: FC<HeaderBarProps> = () => {
    const classes = useStyles();
    return (
        <AppBar className={classes.root} position="static">
            <Toolbar>
                <Logo/>
                <Typography className={classes.title} variant='h6'>STARDATA</Typography>
                <List className={classes.navigation}>
                    <ListItem color='inherit'>
                        <CenteredListItemText>Ranks</CenteredListItemText>
                    </ListItem>
                    <ListItem color='inherit'>
                        <CenteredListItemText>Clubs</CenteredListItemText>
                    </ListItem>
                    <ListItem color='inherit'>
                        <CenteredListItemText>Players</CenteredListItemText>
                    </ListItem>
                </List>
                <Button color="inherit">Login</Button>
            </Toolbar>
        </AppBar>
    )
}

export default HeaderBar
