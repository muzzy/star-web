import React from 'react'
import { FC } from 'react';
import SortBar, { SortItem } from './UI/SortBar'
import { SortBarSizeProps } from './UI/types';

const SeasonSortBar: FC<SortBarSizeProps> = (props) => {
    
    const items = [
        {label: '1996-1997'},
        {label: '1995-1996'}
    ] as SortItem[];

    return (
        <SortBar title='Season' items={items} {...props}/>
    )
}

export default SeasonSortBar;