import { Box, Container, makeStyles } from '@material-ui/core';
import React from 'react';
import {ReactComponent as Logo} from '../logo.svg';

const useStyles = makeStyles(() => ({
    root: {
        color: 'white',
        backgroundColor: 'black',
        height: '25vh'
    }
}));

const Footer = () => {
    const classes = useStyles();
    return (
        <Box className={classes.root}>
            <Container maxWidth='md'>
                <Logo />
            </Container>
        </Box>
    )
};

export default Footer;
