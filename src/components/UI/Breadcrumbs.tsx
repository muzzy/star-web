import { Box, Breadcrumbs, Link, makeStyles } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import React, { FC, ReactNode } from 'react'

interface BreadcrumbsItem {
    name: string,
    target: string
}

export interface BreadcrumbsProps {
    links: BreadcrumbsItem[],
    separator?: ReactNode,
    title?: string,
    description?: string
}

// #0c0c0c

const useStyles = makeStyles(() => ({
    root:{
        display: 'flex',
        backgroundColor: '#0c0c0c',
        color: '#646464',
        alignItems: 'center',
        justifyContent: 'center'
    },
    path: {
        width: '75vw',
        color: 'inherit'
    },
    contentContainer: {
        color: 'white',
        backgroundColor: 'black',
        height: '20vh'
    },
    title: {
        fontSize: '1.5rem'
    }
}));

const Breads : FC<BreadcrumbsProps> = ({links, separator, description, title}) => {

    const classes = useStyles();

    return (
        <>
            <Box className={classes.root} p='.5rem'>
                <Breadcrumbs className={classes.path} separator={separator} aria-label='breadcrumb'>
                    {links.map((link, index) => 
                    <Link key={`bread-item-${index}`} color='inherit' href={link.target}>
                        {link.name}
                    </Link>)}
                </Breadcrumbs>
            </Box>
            {(!!title || !!description) && (
            <Box className={classes.contentContainer} px='15vw' py={2}>
                <Box className={classes.title} mb='1rem'>{title}</Box>
                <Box>{description}</Box>
            </Box>)}
        </>
    )
};

Breads.defaultProps = {
    separator: <NavigateNextIcon fontSize="small" />
}

export default Breads;
