import React from 'react';
import { TableRow, TableBody, TableCell, withStyles } from '@material-ui/core';
import { Record, TableProps } from './types';

const StyledTableCell = withStyles({
    root: {
        color: 'inherit',
        backgroundColor: '#2e2e2e',
        borderBlockColor: '#141414',
        borderWidth: '.5rem'
    }
})(TableCell);

const SDTableBody = <T extends Record, K extends keyof T>({data, fields, name}: TableProps<T, K>) => {

    const rows = data.map((row, index) => (
        <TableRow key={`${name}-${row.id}`}>
            {
                fields.map((field, fIdx) => (
                    <StyledTableCell key={`${name}-${row.id}-${fIdx}`}>
                        {row[field.key]}
                    </StyledTableCell>
                ))
            }
        </TableRow>
    ));

    return (
        <TableBody>
            {rows}
        </TableBody>
    )
}

export default SDTableBody
