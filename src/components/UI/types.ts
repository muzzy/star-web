import { GridSize } from "@material-ui/core"

export interface Record{
    id: number;
    [key: string]: any
}

export type FiledType<T extends Record, K extends keyof T> = {
    key: K;
    label: string;
}

export type TableProps<T extends Record, K extends keyof T> ={
    data: Array<T>;
    fields: Array<FiledType<T, K>>;
    name: string;
}

export interface Player extends Record {
    name: string,
    age: number,
    point: number
}

export interface SortBarSizeProps {
    xs?: boolean | GridSize
}