import { Box, Button, Grid, makeStyles, Typography } from '@material-ui/core'
import React, { FC } from 'react'
import { SortBarSizeProps } from './types';

export interface SortItem {
    label: string
}

type SortBarProps =  {
    title?: string,
    items: SortItem[],
    // classesOverride?: ClassesOverride<typeof useStyles>;
} & SortBarSizeProps

const useStyles = makeStyles({
    grid: {
        marginBottom: '1rem'
    },
    button: {
        color:'white',
        backgroundColor: '#292929',
        '&:hover': {
            backgroundColor: 'white',
            color: 'black'
        }
    }
});

const SortBar: FC<SortBarProps> = ({title, items, ...rest}) => {
    const classes = useStyles();
    return (
        <Grid item {...rest} className={classes.grid}>
            <Typography variant='h6'>{title}</Typography>
            {
                items.map((item, index) =>(
                    <Box key={`box-${title}-${index}`} component='div' display='inline' mr={1}>
                        <Button className={classes.button} variant="contained" color='inherit' key={`${title}-${index}`}>
                            {item.label}
                        </Button>
                    </Box>
                ))
            }
        </Grid>
    )
};

export default SortBar
