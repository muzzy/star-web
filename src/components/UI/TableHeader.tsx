import React from 'react';
import { TableHead, TableRow, TableCell, withStyles } from '@material-ui/core';
import { Record, FiledType } from './types';

type TableHeaderProps<T extends Record, K extends keyof T> = {
    name: string;
    fields: Array<FiledType<T, K>>;
}

const HeaderCell = withStyles({
    root: {
        color: '#646464',
        border:'none'
    }
})(TableCell);

const TableHeader = <T extends Record, K extends keyof T>({fields, name}: TableHeaderProps<T, K>) => {
    const headers = fields.map((field, index) => (
        <HeaderCell key={`${name}-${index}`}>
            {field.label}
        </HeaderCell>
    ));
    return (
        <TableHead>
            <TableRow>{headers}</TableRow>
        </TableHead>
    )
}

export default TableHeader
