import React from 'react';
import {Table, TableContainer, withStyles } from '@material-ui/core';
import TableBody from './SDTableBody';
import TableHeader from './TableHeader';
import {Pagination} from '@material-ui/lab';
import { Record, TableProps } from './types';

const SDPagination = withStyles({
    ul: {
        justifyContent: 'center',
        '& .MuiPaginationItem-root': {
            color: 'white',
            background: '#2e2e2e',
            height:'40px',
            width: '40px'
        },
        '& .MuiPaginationItem-ellipsis':{
            background: 'transparent',
            
        },
        '& li button': {
            border: 'none',
            '&:hover': {
                backgroundColor: '#b6132e'
            }
        },
        '& .Mui-selected':{
            backgroundColor: '#b6132e',
            '&:hover':{
                backgroundColor: '#b6132e'
            }
        }
   }
})(Pagination);

// const StyledCell = withStyles({
//     root: {
//         border: 'none',
//     }
// })(TableCell);

const DataTable = <T extends Record, K extends keyof T>({fields, name, ...rest}: TableProps<T, K>) => {
    // const [page, setPage] = React.useState(0);
    // const [rowsPerPage, setRowsPerPage] = React.useState(10);

    // const handleChangePage = (
    //     event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
    //     newPage: number) => {
    //     setPage(newPage);
    // };

    // const handleChangeRowsPerPage = (
    //     event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    //     setRowsPerPage(+event.target.value);
    //     setPage(0);
    // };
    return (
        <TableContainer>
            <Table>
                <TableHeader fields={fields} name={name} />
                <TableBody fields={fields} name={name} {...rest} />
            </Table>
            <SDPagination count={10} variant="outlined" shape="rounded" />
        </TableContainer>
    )
};

export default DataTable;
