import React, { FC } from 'react'
import SortBar, { SortItem } from './UI/SortBar'
import { SortBarSizeProps } from './UI/types';

const LeagueSortBar: FC<SortBarSizeProps> = (props) => {
    
    const items = [
        {label: 'Premier League'}
    ] as SortItem[];

    return (
        <SortBar title='League' items={items}  {...props}/>
    )
}

export default LeagueSortBar;
