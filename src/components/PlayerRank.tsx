import React, {FC} from 'react';
import DataTable from './UI/DataTable';
import { FiledType, Player } from './UI/types';
import {MockPlayers} from '../mock/Players';



const fields: FiledType<Player, keyof Player>[] = [
    {
        key: 'name',
        label: 'Name'
    },
    {
        key: 'age',
        label: 'Age'
    },
    {
        key: 'point',
        label: 'Point'
    }
];

interface PlayerRankProps {
    
}

const PlayerRank: FC<PlayerRankProps> = () => {

    // const [{data, loading}] = useAxios('/matches/1');

    // if(loading) return <p>Loading</p>
    
    return <DataTable data = {MockPlayers} fields={fields} name='player' />
};

export default PlayerRank;
