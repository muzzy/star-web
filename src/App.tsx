import React from 'react';
import Home from './pages/Home';
import './api/Config';
import { ThemeProvider } from '@material-ui/core';
import sdTheme from './styles/Theme';

function App() {
  return (
    <ThemeProvider theme={sdTheme}>
      <Home />
    </ThemeProvider>
  );
}

export default App;
