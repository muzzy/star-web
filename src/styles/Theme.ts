import { createTheme } from "@material-ui/core";

const theme = createTheme({
    palette: {
        primary: {
            main: '#2e2e2e'
        }
    }
});

export default theme;