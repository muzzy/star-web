import { Box, Container, Grid } from '@material-ui/core';
import React from 'react';
import Footer from '../components/Footer';
import HeaderBar from '../components/HeaderBar';
import LeagueSortBar from '../components/LeagueSortBar';
import PlayerRank from '../components/PlayerRank';
import SeasonSortBar from '../components/SeasonSortBar';
import Breadcrumbs from '../components/UI/Breadcrumbs';
import SortBar from '../components/UI/SortBar';

const Home = () => {
    const links = [{name: 'Home', target: '#'}, {name: 'Rankings', target: '/rankings'}];
    const title='Player Rankings';
    const description = 'Rating driven by ELO algorithm';

    return (
        <div>
            <HeaderBar />
            <Breadcrumbs links={links} description={description} title={title}/>
            
            <Box bgcolor='#141414' color='white' py='2vh'>
                <Container maxWidth='md'>
                    <Grid container>
                        <SortBar items={[{label: 'Players'}, {label: 'Clubs'}]} xs={12} />
                        <LeagueSortBar xs={6} />
                        <SeasonSortBar xs={6} />
                        <PlayerRank />
                    </Grid>
                </Container>
            </Box>
            <Footer />
        </div>
    )
};

export default Home;
