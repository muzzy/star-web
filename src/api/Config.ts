import Axios from "axios";
import { configure } from "axios-hooks";
import LRU from 'lru-cache';

const axios = Axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    headers:{
        'Accept': 'application/json'
    }
});

const cache = new LRU({max: 10});
configure({axios, cache});