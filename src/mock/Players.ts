import Faker from 'faker';
import { Player } from '../components/UI/types';

const CreatePlayer = (): Player => {
    return {
        id: Faker.datatype.number(),
        name: Faker.name.findName(),
        age: Faker.datatype.number(),
        point: Faker.datatype.number()
    }
}

export const MockPlayers = [
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer(),
    CreatePlayer()
] as Player[];